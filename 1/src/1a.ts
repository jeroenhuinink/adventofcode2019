import * as fs from "fs";

function readInput(): string[] {
  return fs
    .readFileSync("input.txt") //read file into buffer
    .toString() //convert buffer to string
    .split("\n") //split lines
    .filter((l) => l); //remove empty lines
}

function parseInput(input: string[]): number[] {
  return input.map((s) => +s); //convert to integer
}

function massToFuel(mass: number) {
  return Math.floor(mass / 3) - 2;
}

function solve(data: number[]): number {
  return data.map(massToFuel).reduce((a, c) => a + c, 0);
}

let testNumber = 0;
function test(input: string[], expected: number) {
  testNumber++;
  const actual = massToFuel(+input[0]);
  if (actual !== expected) {
    throw `Test ${testNumber} failed`;
  }
}

function test2(input: string[], expected: number) {}
test(["12"], 2);
test(["14"], 2);
test(["1969"], 654);
test(["100756"], 33583);

test2(["12", "14", "1969", "100756"], 2 + 2 + 654 + 33583);

console.info(solve(parseInput(readInput())));
