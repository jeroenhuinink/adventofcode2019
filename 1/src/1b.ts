import * as fs from "fs";

function readInput(): string[] {
  return fs
    .readFileSync("input.txt") //read file into buffer
    .toString() //convert buffer to string
    .split("\n") //split lines
    .filter((l) => l); //remove empty lines
}

function parseInput(input: string[]): number[] {
  return input.map((s) => +s); //convert to integer
}

function massToFuel(mass: number) {
  return Math.floor(mass / 3) - 2;
}

function includeFuelWeight(mass: number): number {
  let totalMass = mass;
  let fuel = mass;
  do {
    fuel = massToFuel(fuel);
    if (fuel > 0) {
      totalMass = totalMass + fuel;
    }
  } while (fuel > 0);
  return totalMass;
}

function solve(data: number[]): number {
  return data
    .map((m) => includeFuelWeight(massToFuel(m)))
    .reduce((a, c) => a + c, 0);
}

let testNumber = 0;
function test(input: string[], expected: number) {
  testNumber++;
  const actual = includeFuelWeight(massToFuel(+input[0]));
  if (actual !== expected) {
    throw `Test ${testNumber} failed, ${expected} was ${actual}`;
  }
}

function test2(input: string[], expected: number) {}
test(["12"], 2);
test(["1969"], 966);
test(["100756"], 50346);

test2(["12", "1969", "100756"], 2 + 966 + 33583);

console.info(solve(parseInput(readInput())));
