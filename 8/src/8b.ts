import * as fs from "fs";

function readFile(): string {
  return fs.readFileSync("input.txt").toString();
}

function readInput(file: string): number[] {
  return file
    .split("")
    .filter((l) => l)
    .map((s) => +s);
}

function solve(input: number[], width: number, height: number): string[] {
  const layerCount = input.length / (width * height);
  let current = 0;
  const layers = [];
  for (let i = 0; i < layerCount; i++) {
    layers[i] = [];
    for (let j = 0; j < height; j++) {
      layers[i][j] = [];
      for (let k = 0; k < width; k++) {
        layers[i][j][k] = input[current];
        current++;
      }
    }
  }
  const image = [];
  for (let i = 0; i < layerCount; i++) {
    for (let j = 0; j < height; j++) {
      if (!image[j]) {
        image[j] = [];
      }
      for (let k = 0; k < width; k++) {
        if (image[j][k] == undefined) {
          image[j][k] = 2;
        }
        if (image[j][k] == 2) {
          image[j][k] = layers[i][j][k];
        }
      }
    }
  }

  return image.map((row) => row.map((c) => (c == 0 ? " " : "*")).join(""));
}

let testNumber = 0;
function test(input: string, width: number, height: number, expected: string) {
  testNumber++;
  const actual = solve(readInput(input), width, height).join("");
  if (actual !== expected) {
    throw `Test ${testNumber} failed, ${expected} was ${actual}`;
  }
}

test(`0222112222120000`, 2, 2, ` ** `);

console.info(solve(readInput(readFile()), 25, 6));
