import * as fs from "fs";

function readFile(): string {
  return fs.readFileSync("input.txt").toString();
}

function readInput(file: string): number[] {
  return file
    .split("")
    .filter((l) => l)
    .map((s) => +s);
}

function solve(input: number[], width: number, height: number): number {
  let currentZeros = 0;
  let currentOnes = 0;
  let currentTwos = 0;
  let minZeros = Infinity;
  let current = 0;
  let result = 0;
  while (current < input.length) {
    if (input[current] == 0) {
      currentZeros += 1;
    } else if (input[current] == 1) {
      currentOnes += 1;
    } else if (input[current] == 2) {
      currentTwos += 1;
    }
    current++;
    if (current % (width * height) == 0) {
      if (currentZeros < minZeros) {
        minZeros = currentZeros;
        result = currentOnes * currentTwos;
      }
      currentZeros = 0;
      currentOnes = 0;
      currentTwos = 0;
    }
  }
  return result;
}

let testNumber = 0;
function test(input: string, width: number, height: number, expected: number) {
  testNumber++;
  const actual = solve(readInput(input), width, height);
  if (actual !== expected) {
    throw `Test ${testNumber} failed, ${expected} was ${actual}`;
  }
}

test(`123456189012`, 3, 2, 1);
test(`011222333444`, 3, 2, 0);
test(`001333012222000321`, 3, 2, 4);

console.info(solve(readInput(readFile()), 25, 6));
