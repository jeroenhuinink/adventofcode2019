import * as fs from "fs";

function exec(p: number[]) {
  let c = 0;
  let finished = false;

  const add = () => {
    p[p[c + 3]] = p[p[c + 1]] + p[p[c + 2]];
  };

  const mult = () => {
    p[p[c + 3]] = p[p[c + 1]] * p[p[c + 2]];
  };

  const next = () => {
    c += 4;
  };

  const finish = () => {
    finished = true;
  };

  const opcodes = {
    1: add,
    2: mult,
    99: finish
  };

  while (!finished) {
    opcodes[p[c]]();
    next();
  }
  return p;
}

function readFile(): string {
  return fs.readFileSync("input.txt").toString();
}

function readInput(file: string): string[] {
  return file.split(",").filter((l) => l);
}

function parseInput(input: string[]): number[] {
  return input.map((s) => +s);
}

function solve(data: number[]): number[] {
  return exec(data);
}

let testNumber = 0;
function test(input: string, expected: number[]) {
  function arraysEqual<T>(a: T[], b: T[]) {
    if (a === b) return true;
    if (a == null || b == null) {
      return false;
    }
    if (a.length != b.length) {
      return false;
    }

    for (var i = 0; i < a.length; ++i) {
      if (a[i] !== b[i]) {
        return false;
      }
    }
    return true;
  }

  testNumber++;
  const actual = solve(parseInput(readInput(input)));
  if (!arraysEqual(actual, expected)) {
    throw `Test ${testNumber} failed, ${expected} was ${actual}`;
  }
}

test(`1,0,0,0,99`, [2, 0, 0, 0, 99]);

test(`2,3,0,3,99`, [2, 3, 0, 6, 99]);

test(`2,4,4,5,99`, [2, 4, 4, 5, 99, 9801]);

test(`1,1,1,4,99,5,6,0,99`, [30, 1, 1, 4, 2, 5, 6, 0, 99]);

const input = parseInput(readInput(readFile()));
input[1] = 12;
input[2] = 2;
console.info(solve(input)[0]);
