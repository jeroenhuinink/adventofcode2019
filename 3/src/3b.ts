import * as fs from "fs";

function readFile(): string {
  return fs.readFileSync("input.txt").toString();
}

function readInput(file: string): string[] {
  return file.split("\n").filter((l) => l);
}

interface Direction {
  dx: number;
  dy: number;
}

interface Path {
  direction: Direction;
  count: number;
}

const directions = {
  R: { dx: 1, dy: 0 },
  L: { dx: -1, dy: 0 },
  U: { dx: 0, dy: 1 },
  D: { dx: 0, dy: -1 }
};

const parseDirection = (d: string) => ({
  direction: directions[d.substr(0, 1)],
  count: +d.substr(1)
});

function parseInput(input: string[]): { green: Path[]; red: Path[] } {
  const green: Path[] = input[0].split(",").map(parseDirection);
  const red = input[1].split(",").map(parseDirection);
  return { green, red };
}

function solve(input: { green: Path[]; red: Path[] }): number {
  const { green, red } = input;
  const grid: { occup: string; redSteps: number; greenSteps: number }[][] = [];
  const origin = { x: 5000, y: 5000 };
  let current = { ...origin };
  grid[0] = [];
  grid[0][0] = { occup: "s", redSteps: 0, greenSteps: 0 };
  let steps = 0;
  green.forEach((step: Path) => {
    const { direction, count } = step;
    let i = count;
    while (i > 0) {
      steps++;
      current.x = current.x + direction.dx;
      current.y = current.y + direction.dy;

      if (!grid[current.x]) {
        grid[current.x] = [];
      }

      grid[current.x][current.y] = {
        occup: "g",
        redSteps: 0,
        greenSteps: steps
      };

      i--;
    }
  });

  let crossings: { x: number; y: number }[] = [];
  current = { ...origin };
  steps = 0;
  red.forEach((step: Path) => {
    const { direction, count } = step;
    let i = count;
    while (i > 0) {
      steps++;
      current.x = current.x + direction.dx;
      current.y = current.y + direction.dy;

      if (!grid[current.x]) {
        grid[current.x] = [];
      }
      if (
        grid[current.x][current.y] &&
        grid[current.x][current.y].greenSteps > 0 &&
        grid[current.x][current.y].redSteps == 0
      ) {
        grid[current.x][current.y].occup = "x";
        grid[current.x][current.y].redSteps = steps;
        crossings.push({ x: current.x, y: current.y });
      } else {
        grid[current.x][current.y] = {
          occup: "r",
          redSteps: steps,
          greenSteps: 0
        };
      }
      i--;
    }
  });

  return crossings.reduce((a, pos: { x: number; y: number }) => {
    const { x, y } = pos;
    const { redSteps, greenSteps } = grid[x][y];
    const c = redSteps + greenSteps;
    return c < a ? c : a;
  }, Infinity);
}

let testNumber = 0;
function test(input: string, expected: number) {
  testNumber++;
  const actual = solve(parseInput(readInput(input)));
  if (actual != expected) {
    throw `Test ${testNumber} failed, ${expected} was ${actual}`;
  }
}

test(
  `R8,U5,L5,D3
U7,R6,D4,L4`,
  30
);

test(
  `R75,D30,R83,U83,L12,D49,R71,U7,L72
U62,R66,U55,R34,D71,R55,D58,R83`,
  610
);

test(
  `R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51
U98,R91,D20,R16,D67,R40,U7,R15,U6,R7`,
  410
);

console.info(solve(parseInput(readInput(readFile()))));
