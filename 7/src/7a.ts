import * as fs from "fs";
import { exec } from "../../intCode";

function readFile(): string {
  return fs.readFileSync("input.txt").toString();
}

function readInput(file: string): string[] {
  return file.split(",").filter((l) => l);
}

function parseInput(input: string[]): number[] {
  return input.map((s) => +s);
}

function processSignal(program: number[], phases: number[]): number {
  const output1 = exec([...program], [phases[0], 0]).outputs[0];
  const output2 = exec([...program], [phases[1], output1]).outputs[0];
  const output3 = exec([...program], [phases[2], output2]).outputs[0];
  const output4 = exec([...program], [phases[3], output3]).outputs[0];
  const output5 = exec([...program], [phases[4], output4]).outputs[0];
  return output5;
}

// from https://stackoverflow.com/a/37580979
function permute(permutation: number[]) {
  const length = permutation.length;
  const result = [permutation.slice()];
  const c = new Array(length).fill(0);
  let i = 1;
  let k: number;
  let p: number;

  while (i < length) {
    if (c[i] < i) {
      k = i % 2 && c[i];
      p = permutation[i];
      permutation[i] = permutation[k];
      permutation[k] = p;
      ++c[i];
      i = 1;
      result.push(permutation.slice());
    } else {
      c[i] = 0;
      ++i;
    }
  }
  return result;
}

const possiblePermutations = permute([0, 1, 2, 3, 4]);

const max = (a: number[]) => a.reduce((a, c) => (c > a ? c : a), 0);

function solve(data: number[]): number {
  return max(
    possiblePermutations.map((setting) => processSignal(data, setting))
  );
}

let testNumber = 0;
function test(input: string, expected: number) {
  testNumber++;
  const actual = solve(parseInput(readInput(input)));
  if (actual !== expected) {
    throw `Test ${testNumber} failed, ${expected} was ${actual}`;
  }
}

test(`3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0`, 43210);

test(
  `3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0`,
  54321
);

test(
  `3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0`,
  65210
);

const input = parseInput(readInput(readFile()));

console.info(solve(input));
