export function exec(
  p: number[],
  inputs: number[]
): { memory: number[]; outputs: number[] } {
  let relativeBase = 0;
  let programCounter = 0;
  let finished = false;

  const getAddress = (mode: number[], counter: number, offset: number) => {
    return mode[offset - 1] == 2
      ? p[counter + offset] + relativeBase
      : mode[offset - 1] == 1
      ? counter + offset
      : p[counter + offset];
  };

  const getValue = (m: number[], c: number, pos: number) =>
    p[getAddress(m, c, pos)] || 0;

  const add = (m: number[]) => {
    p[getAddress(m, programCounter, 3)] =
      getValue(m, programCounter, 1) + getValue(m, programCounter, 2);
    programCounter += 4;
  };

  const mult = (m: number[]) => {
    p[getAddress(m, programCounter, 3)] =
      getValue(m, programCounter, 1) * getValue(m, programCounter, 2);
    programCounter += 4;
  };

  const iftrue = (m: number[]) => {
    if (p[getAddress(m, programCounter, 1)] != 0) {
      programCounter = getValue(m, programCounter, 2);
    } else {
      programCounter += 3;
    }
  };
  const iffalse = (m: number[]) => {
    if (p[getAddress(m, programCounter, 1)] == 0) {
      programCounter = getValue(m, programCounter, 2);
    } else {
      programCounter += 3;
    }
  };

  const lessThan = (m: number[]) => {
    p[getAddress(m, programCounter, 3)] =
      getValue(m, programCounter, 1) < getValue(m, programCounter, 2) ? 1 : 0;
    programCounter += 4;
  };

  const equals = (m: number[]) => {
    p[getAddress(m, programCounter, 3)] =
      getValue(m, programCounter, 1) == getValue(m, programCounter, 2) ? 1 : 0;
    programCounter += 4;
  };

  const offset = (m: number[]) => {
    relativeBase += getValue(m, programCounter, 1);
    programCounter += 2;
  };

  let inputCounter = 0;

  const input = (m: number[]) => {
    p[getAddress(m, programCounter, 1)] = inputs[inputCounter];
    inputCounter++;
    programCounter += 2;
  };

  const outputs = [];
  const output = (m: number[]) => {
    outputs.push(p[getAddress(m, programCounter, 1)]);
    programCounter += 2;
  };

  const finish = () => {
    finished = true;
  };

  const opcodes = {
    1: add,
    2: mult,
    3: input,
    4: output,
    5: iftrue,
    6: iffalse,
    7: lessThan,
    8: equals,
    9: offset,
    99: finish
  };

  const parseInstruction = (p: number) => {
    const digits = p
      .toString()
      .split("")
      .map((s) => +s);
    const l = digits.length;
    const m = [digits[l - 3] || 0, digits[l - 4] || 0, digits[l - 5] || 0]; //modes
    const i = (digits[l - 2] || 0) * 10 + digits[l - 1]; //instruction
    return { i, m };
  };
  while (!finished) {
    const { i, m } = parseInstruction(p[programCounter]);
    opcodes[i](m);
  }
  return { memory: p, outputs };
}
