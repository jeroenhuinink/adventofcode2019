import * as fs from "fs";

type Graph = { [node: string]: { [neighbor: string]: number } };

// Dijkstra function from: https://gist.github.com/stella-yc/49a7b96679ab3bf06e26421fc81b5636
// Added types
const lowestCostNode = (
  costs: { [key: string]: number },
  processed: string[]
) => {
  return Object.keys(costs).reduce((lowest, node) => {
    if (lowest === null || costs[node] < costs[lowest]) {
      if (!processed.includes(node)) {
        lowest = node;
      }
    }
    return lowest;
  }, null);
};

// function that returns the minimum cost and path to reach Finish
const dijkstra = (graph: Graph) => {
  // track lowest cost to reach each node
  const costs = Object.assign({ finish: Infinity }, graph.start);

  // track paths
  const parents = { finish: null };
  for (let child in graph.start) {
    parents[child] = "start";
  }

  // track nodes that have already been processed
  const processed = [];

  let node = lowestCostNode(costs, processed);

  while (node) {
    let cost = costs[node];
    let children = graph[node];
    for (let n in children) {
      let newCost = cost + children[n];
      if (!costs[n]) {
        costs[n] = newCost;
        parents[n] = node;
      }
      if (costs[n] > newCost) {
        costs[n] = newCost;
        parents[n] = node;
      }
    }
    processed.push(node);
    node = lowestCostNode(costs, processed);
  }

  let optimalPath = ["finish"];
  let parent = parents.finish;
  while (parent) {
    optimalPath.push(parent);
    parent = parents[parent];
  }
  optimalPath.reverse();

  const results = {
    distance: costs.finish,
    path: optimalPath
  };

  return results;
};

function readFile(): string {
  return fs.readFileSync("input.txt").toString();
}

function readInput(file: string): string[] {
  return file.split("\n").filter((l) => l);
}

function parseInput(input: string[]): Graph {
  const satelites: Graph = {};

  input
    .map((s) => s.split(")"))
    .forEach((p) => {
      satelites[p[0]] = { ...satelites[p[0]], [p[1]]: 1 };
      satelites[p[1]] = { ...satelites[p[1]], [p[0]]: 1 };
    });

  return satelites;
}

function solve(satelites: Graph): number {
  const solution = dijkstra({
    ...satelites,
    start: { YOU: 1 },
    SAN: { ...satelites["SAN"], finish: 1 },
    finish: {}
  });

  return solution.distance - 4;
}

let testNumber = 0;
function test(input: string, expected: number) {
  testNumber++;
  const actual = solve(parseInput(readInput(input)));
  if (actual !== expected) {
    throw `Test ${testNumber} failed, ${expected} was ${actual}`;
  }
}

test(
  `COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L
K)YOU
I)SAN`,
  5
);

console.info(solve(parseInput(readInput(readFile()))));
