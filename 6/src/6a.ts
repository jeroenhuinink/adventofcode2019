import * as fs from "fs";

const sum = (a: number[]) => a.reduce((a, c) => a + c, 0);

function readFile(): string {
  return fs.readFileSync("input.txt").toString();
}

function readInput(file: string): string[] {
  return file.split("\n").filter((l) => l);
}

function parseInput(input: string[]): { [key: string]: string } {
  const orbits: { [key: string]: string } = {};
  input
    .map((s) => s.split(")"))
    .forEach((p) => {
      orbits[p[1]] = p[0];
    });

  return orbits;
}

function solve(orbits: { [key: string]: string }): number {
  function getOrbits(outer: string): number {
    const inner = orbits[outer];
    if (!inner) {
      return 0;
    }

    return 1 + getOrbits(inner);
  }

  return sum(Object.keys(orbits).map(getOrbits));
}

let testNumber = 0;
function test(input: string, expected: number) {
  testNumber++;
  const actual = solve(parseInput(readInput(input)));
  if (actual !== expected) {
    throw `Test ${testNumber} failed, ${expected} was ${actual}`;
  }
}

test(
  `COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L`,
  42
);

console.info(solve(parseInput(readInput(readFile()))));
