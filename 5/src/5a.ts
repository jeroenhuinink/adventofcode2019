import * as fs from "fs";

// function exec(p: number[], inputs: number[]): number[] {
//   let c = 0;
//   let finished = false;

//   const ga = (mode: number[], counter: number, offset: number) => {
//     return mode[offset - 1] ? counter + offset : p[counter + offset];
//   };
//   const add = (m: number[]) => {
//     p[ga(m, c, 3)] = p[ga(m, c, 1)] + p[ga(m, c, 2)];
//   };

//   const mult = (m: number[]) => {
//     p[ga(m, c, 3)] = p[ga(m, c, 1)] * p[ga(m, c, 2)];
//   };

//   const inputCounter = 0;

//   const input = (m: number[]) => {
//     console.log("in", inputs[inputCounter]);
//     p[ga(m, c, 1)] = inputs[inputCounter];
//   };

//   const outputs = [];
//   const output = (m: number[]) => {
//     console.log("out", p[ga(m, c, 1)]);
//     outputs.push(p[ga(m, c, 1)]);
//   };

//   const next = (size: number) => {
//     c += size;
//   };

//   const finish = () => {
//     finished = true;
//   };

//   const opcodes = {
//     1: add,
//     2: mult,
//     3: input,
//     4: output,
//     99: finish
//   };

//   const size = {
//     1: 4,
//     2: 4,
//     3: 2,
//     4: 2,
//     99: 0
//   };
//   const parseInstruction = (p: number) => {
//     const digits = p
//       .toString()
//       .split("")
//       .map((s) => +s);
//     const l = digits.length;
//     const m = [digits[l - 3] || 0, digits[l - 4] || 0, digits[l - 5] || 0];
//     const i = (digits[l - 2] || 0) * 10 + digits[l - 1];
//     const s = size[i];
//     return { i, s, m };
//   };
//   while (!finished) {
//     const { i, m, s } = parseInstruction(p[c]);
//     console.log({ c, i, m, s, p });
//     opcodes[i](m);
//     next(s);
//   }
//   return outputs;
// }

import { exec } from "../../intCode";

function readFile(): string {
  return fs.readFileSync("input.txt").toString();
}

function readInput(file: string): string[] {
  return file.split(",").filter((l) => l);
}

function parseInput(input: string[]): number[] {
  return input.map((s) => +s);
}

function solve(data: number[], inputs: number[]): number[] {
  return exec(data, inputs).outputs;
}

let testNumber = 0;
function test(input: string, inputs: number[], expected: number[]) {
  function arraysEqual<T>(a: T[], b: T[]) {
    if (a === b) return true;
    if (a == null || b == null) {
      return false;
    }
    if (a.length != b.length) {
      return false;
    }

    for (var i = 0; i < a.length; ++i) {
      if (a[i] !== b[i]) {
        return false;
      }
    }
    return true;
  }

  testNumber++;
  const actual = solve(parseInput(readInput(input)), inputs);
  if (!arraysEqual(actual, expected)) {
    throw `Test ${testNumber} failed, ${expected} was ${actual}`;
  }
}

test(`3,0,4,0,99`, [12], [12]);
test(`3,0,4,5,99,13`, [12], [13]);
test(`3,5,4,5,99,13`, [12], [12]);
test(`3,5,104,14,99,13`, [13], [14]);
test(`3,1,1,0,1,9,4,9,99`, [13], [16]);
test(`3,1,1101,2,2,0,4,0,99`, [13], [4]);
test(`3,1,11101,2,2,0,4,5,99`, [13], [4]);
test(`3,1,1001,1,3,0,4,0,99`, [13], [16]);
// test(`1,0,0,0,99`, [2, 0, 0, 0, 99], []);

// test(`2,3,0,3,99`, [2, 3, 0, 6, 99], []);

// test(`2,4,4,5,99`, [2, 4, 4, 5, 99, 9801], []);

// test(`1,1,1,4,99,5,6,0,99`, [30, 1, 1, 4, 2, 5, 6, 0, 99], []);

const input = parseInput(readInput(readFile()));

console.info(solve(input, [1]).slice(-1)[0]);
