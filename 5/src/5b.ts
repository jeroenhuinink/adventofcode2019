import * as fs from "fs";
import { exec } from "../../intCode";

function readFile(): string {
  return fs.readFileSync("input.txt").toString();
}

function readInput(file: string): string[] {
  return file.split(",").filter((l) => l);
}

function parseInput(input: string[]): number[] {
  return input.map((s) => +s);
}

function solve(data: number[], inputs: number[]): number[] {
  return exec(data, inputs).outputs;
}

let testNumber = 0;
function test(input: string, inputs: number[], expected: number[]) {
  function arraysEqual<T>(a: T[], b: T[]) {
    if (a === b) return true;
    if (a == null || b == null) {
      return false;
    }
    if (a.length != b.length) {
      return false;
    }

    for (var i = 0; i < a.length; ++i) {
      if (a[i] !== b[i]) {
        return false;
      }
    }
    return true;
  }

  testNumber++;
  const actual = solve(parseInput(readInput(input)), inputs);
  if (!arraysEqual(actual, expected)) {
    throw `Test ${testNumber} failed, ${expected} was ${actual}`;
  }
}

test(`3,0,4,0,99`, [12], [12]);
test(`3,0,4,5,99,13`, [12], [13]);
test(`3,5,4,5,99,13`, [12], [12]);
test(`3,5,104,14,99,13`, [13], [14]);
test(`3,1,1,0,1,9,4,9,99`, [13], [16]);
test(`3,1,1101,2,2,0,4,0,99`, [13], [4]);
test(`3,1,11101,2,2,0,4,5,99`, [13], [4]);
test(`3,1,1001,1,3,0,4,0,99`, [13], [16]);
test(`3,9,8,9,10,9,4,9,99,-1,8`, [8], [1]);
test(`3,9,8,9,10,9,4,9,99,-1,8`, [9], [0]);
test(`3,3,1108,-1,8,3,4,3,99`, [8], [1]);
test(`3,3,1108,-1,8,3,4,3,99`, [9], [0]);
test(`3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9`, [0], [0]);
test(`3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9`, [11], [1]);
test(`3,3,1105,-1,9,1101,0,0,12,4,12,99,1`, [0], [0]);
test(`3,3,1105,-1,9,1101,0,0,12,4,12,99,1`, [-1], [1]);
test(
  `3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99`,
  [7],
  [999]
);
test(
  `3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99`,
  [8],
  [1000]
);
test(
  `3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99`,
  [9],
  [1001]
);
const input = parseInput(readInput(readFile()));

console.info(solve(input, [5]).slice(-1)[0]);
