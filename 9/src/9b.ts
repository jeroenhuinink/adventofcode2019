import * as fs from "fs";

import { exec } from "../../intCode";

function readFile(): string {
  return fs.readFileSync("input.txt").toString();
}

function readInput(file: string): string[] {
  return file.split(",").filter((l) => l);
}

function parseInput(input: string[]): number[] {
  return input.map((s) => +s);
}

function solve(data: number[], inputs: number[]): number[] {
  const { memory, outputs } = exec(data, inputs);
  return outputs;
}

let testNumber = 0;
function test(input: string, inputs: number[], expected: number[]) {
  function arraysEqual<T>(a: T[], b: T[]) {
    if (a === b) return true;
    if (a == null || b == null) {
      return false;
    }
    if (a.length != b.length) {
      return false;
    }

    for (var i = 0; i < a.length; ++i) {
      if (a[i] !== b[i]) {
        return false;
      }
    }
    return true;
  }

  testNumber++;
  const actual = solve(parseInput(readInput(input)), inputs);
  if (!arraysEqual(actual, expected)) {
    throw `Test ${testNumber} failed, ${expected} was ${actual}`;
  }
}

test(
  `109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99`,
  [],
  [109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99]
);
test(`1102,34915192,34915192,7,4,7,99,0`, [], [34915192 * 34915192]);
test(`104,1125899906842624,99`, [], [1125899906842624]);

const input = parseInput(readInput(readFile()));

console.info(solve(input, [2]));
