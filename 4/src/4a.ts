const test = (
  f: (input: number) => boolean,
  input: number,
  expected: boolean
) => {
  const actual = f(input);
  if (actual != expected) {
    throw input.toString();
  }
};

const increasing = (c: number) =>
  c
    .toString()
    .split("")
    .map((s) => +s)
    .reduce((a, c) => (c >= a ? c : 10), 0) != 10;

test(increasing, 111111, true);
test(increasing, 911111, false);
test(increasing, 223450, false);
test(increasing, 123789, true);

const hasDouble = (c: number) =>
  c
    .toString()
    .split("")
    .map((s) => +s)
    .reduce(
      (a, c) =>
        c == a.prev ? { prev: c, double: true } : { prev: c, double: a.double },
      { prev: -1, double: false }
    ).double;

test(hasDouble, 122345, true);
test(hasDouble, 223455, true);
test(hasDouble, 1234567, false);

function valid(c: number) {
  return increasing(c) && hasDouble(c);
}

function solve(min: number, max: number): number {
  let count = 0;
  for (let candidate = min; candidate <= max; candidate++) {
    if (valid(candidate)) {
      count++;
    }
  }
  return count;
}

console.info(solve(248345, 746315));
