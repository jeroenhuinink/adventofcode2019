const test = (
  f: (input: number) => boolean,
  input: number,
  expected: boolean
) => {
  const actual = f(input);
  if (actual != expected) {
    throw input.toString();
  }
};

const increasing = (c: number) =>
  c
    .toString()
    .split("")
    .map((s) => +s)
    .reduce((a, c) => (c >= a ? c : 10), 0) != 10;

test(increasing, 111111, true);
test(increasing, 911111, false);
test(increasing, 223450, false);
test(increasing, 123789, true);

const hasDouble = (c: number) =>
  c
    .toString()
    .split("")
    .map((s) => +s)
    .map(
      (s, i, a) =>
        (!a[i - 1] || s !== a[i - 1]) &&
        s == a[i + 1] &&
        (!a[i + 2] || s !== a[i + 2])
    )
    .reduce((a, c) => a || c, false);

test(hasDouble, 123455, true);
test(hasDouble, 123444, false);
test(hasDouble, 122234, false);
test(hasDouble, 122233, true);
test(hasDouble, 122333, true);
test(hasDouble, 223455, true);
test(hasDouble, 223445, true);
test(hasDouble, 223444, true);
test(hasDouble, 123456, false);

function valid(c: number) {
  return increasing(c) && hasDouble(c);
}

function solve(min: number, max: number): number {
  let count = 0;
  for (let candidate = min; candidate <= max; candidate++) {
    if (valid(candidate)) {
      count++;
    }
  }
  return count;
}

console.info(solve(248345, 746315));
