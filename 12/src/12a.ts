import * as fs from "fs";

type Coord = { x: number; y: number; z: number };

type Moon = { pos: Coord; vel: Coord };

function readFile(): string {
  return fs.readFileSync("input.txt").toString();
}

function readInput(file: string): string[] {
  return file.split("\n").filter((l) => l);
}

function parseInput(lines: string[]): Coord[] {
  return lines.map((line) => {
    const m = line.match(/\<x=([-]?\d+), y=([-]?\d+), z=([-]?\d+)\>/);
    return { x: +m[1], y: +m[2], z: +m[3] };
  });
}

function solve(input: Coord[], steps: number): number {
  function getVelocity(i: number, j: number, coord: string): number {
    if (moons[i].pos[coord] < moons[j].pos[coord]) {
      return +1;
    }
    if (moons[i].pos[coord] > moons[j].pos[coord]) {
      return -1;
    }
    return 0;
  }

  const moons = input.map(
    (i) => ({ pos: i, vel: { x: 0, y: 0, z: 0 } } as Moon)
  );

  for (let step = 0; step < steps; step++) {
    // Gravity
    for (let i = 0; i < moons.length; i++) {
      for (let j = 0; j < moons.length; j++) {
        moons[i].vel.x += getVelocity(i, j, "x");
        moons[i].vel.y += getVelocity(i, j, "y");
        moons[i].vel.z += getVelocity(i, j, "z");
      }
    }

    //Velocity
    for (let i = 0; i < moons.length; i++) {
      const { x, y, z } = moons[i].vel;
      moons[i].pos.x += x;
      moons[i].pos.y += y;
      moons[i].pos.z += z;
    }
  }

  const result = moons.reduce(
    (a, c) =>
      a +
      (Math.abs(c.pos.x) + Math.abs(c.pos.y) + Math.abs(c.pos.z)) *
        (Math.abs(c.vel.x) + Math.abs(c.vel.y) + Math.abs(c.vel.z)),
    0
  );
  return result;
}

let testNumber = 0;
function test(input: string, steps: number, expected: number) {
  testNumber++;
  const actual = solve(parseInput(readInput(input)), steps);
  if (actual !== expected) {
    throw `Test ${testNumber} failed, ${expected} was ${actual}`;
  }
}

test(
  `<x=-1, y=0, z=2>
<x=2, y=-10, z=-7>
<x=4, y=-8, z=8>
<x=3, y=5, z=-1>`,
  10,
  179
);

console.info(solve(parseInput(readInput(readFile())), 1000));
